# README #

# Recommended Hardware #
	* Raspberry PI 3 Model B
	* 16GB Ultra SD Card


# Using an Windows Operational System #
	* Install the "Win32DiskImager-0.9.5-install.exe" located at this folder
	* Download the "raspbian-jessie.img" from "https://downloads.raspberrypi.org/raspbian_latest"
	* With the "Win32DiskImager", burn the file "raspbian-jessie.img" to the SD Card


# Manual things #
	* Insert the SD card into the Raspberry PI and turn it on
	* Connect an rj45 cable on Raspberry PI


# At the network modem #
	* Locate the Raspberry PI IP


# Using an Windows Operational System #
	* Run the "putty.exe" located at this folder an connect into the Raspberry PI
	* Login with pi/raspberry


# Installing TightVNC Server at Raspberry PI #
	* sudo apt-get install tightvncserver
	* tightvncserver
		- password: 12345678
	* nano /home/pi/.vnc/xstartup
		- add "-cursor_name left_ptr" to "xsetroot"
	* sudo su
	* cd /etc/init.d
	* nano vncboot
	
	#! /bin/sh
	# /etc/init.d/vncboot
	### BEGIN INIT INFO
	# Provides: vncboot
	# Required-Start: $remote_fs $syslog
	# Required-Stop: $remote_fs $syslog
	# Default-Start: 2 3 4 5
	# Default-Stop: 0 1 6
	# Short-Description: Start VNC Server at boot time
	# Description: Start VNC Server at boot time.
	### END INIT INFO
	USER=pi
	HOME=/home/pi
	export USER HOME
	case "$1" in
	start)
	echo "Starting VNC Server"
	#Insert your favoured settings for a VNC session
	su - $USER -c "/usr/bin/vncserver :1 -geometry 1280x800 -depth 16 -pixelformat rgb565"
	;;
	stop)
	echo "Stopping VNC Server"
	/usr/bin/vncserver -kill :1
	;;
	*)
	echo "Usage: /etc/init.d/vncboot {start|stop}"
	exit 1
	;;
	esac
	exit 0
	
	* chmod 755 vncboot
	* update-rc.d -f lightdm remove
	* update-rc.d vncboot defaults


# Installing tightvncviewer on Windows #
	* Install the "tightvnc-2.7.10-setup-64bit.msi" located at this folder
	* During the install, uncheck the TightVNC Server Option


# Setting Up Analog audio and Wi-Fi #
	- Go to "Menu >> Preferences >> Raspberry PI Configuration" and uncheck the "Login as user 'pi' checkbox"
	- Run the "TightVNC Viewer" and connect into the Raspberry PI IP
	- Change the audio from HDMI to Analog
	- Connect into Wi-Fi Network
	- Locate the MAC Addres at the modem and fix it for an IP
	- Reboot the modem
	- Change the location settings
	- Reboot the Raspberry PI


# Installing Chromium Dependencies #
	* mkdir chrome
	* cd chrome
	* sudo apt-get install libpci3
	* sudo apt-get install libspeechd2
	* wget http://ftp.acc.umu.se/mirror/cdimage/snapshot/Debian/pool/main/libg/libgcrypt11/libgcrypt11_1.5.3-5_armhf.deb
	* sudo dpkg -i libgcrypt11_1.5.3-5_armhf.deb


# Installing Chromium Browser #
	* wget http://launchpadlibrarian.net/280845438/chromium-browser_52.0.2743.116-0ubuntu0.14.04.1.1134_armhf.deb
	* wget http://launchpadlibrarian.net/280845440/chromium-codecs-ffmpeg-extra_52.0.2743.116-0ubuntu0.14.04.1.1134_armhf.deb
	* sudo dpkg -i chromium-browser_52.0.2743.116-0ubuntu0.14.04.1.1134_armhf.deb
	* sudo dpkg -i chromium-codecs-ffmpeg-extra_52.0.2743.116-0ubuntu0.14.04.1.1134_armhf.deb


# Installing B.I.A. Local Server/Client #
	* wget http://bitbucket.org/ruanbarroso/business-intelligence-analyst/raw/(hash?)/(version?)/bia.jar -O /home/pi/bia.jar
	* nano /home/pi/.config/lxsession/LXDE-pi/autostart
	
	@lxpanel --profile LXDE-pi
	@pcmanfm --desktop --profile LXDE-pi
	#@xscreensaver -no-splash
	@xset s off
	@xset -dpms
	@xset s noblank
	@lxterminal -e sudo java -jar bia.jar IPADDRESS:PORT SERVICE USER PASS CODUSU
	
	* sudo reboot


# Setting Up the chrome #
	* Connect with TightVNC Viewer and open the Chromium Browser (chromium-browser --ignore-certificate-errors)
	* Navigate to http://localhost:8889/index.jsp
	* Add permission to audio recording
	* Select the correct microphone at chrome settings
	* Change Language at chrome settings
	* Reboot the Raspberry PI


# For update #
	* wget http://bitbucket.org/ruanbarroso/business-intelligence-analyst/raw/(hash?)/(version?)/bia.jar -O /home/pi/bia.jar
	* sudo reboot


# -------- Setting-up the Business Intelligence Analyst Use Cases -------- #

## Pre imported classes ##
	import java.math.BigDecimal;
	import java.util.ArrayList;
	import java.util.HashMap;
	import java.util.Map;
	import java.util.Map.Entry;
	import java.util.Iterator;
	import java.util.regex.Pattern;
	import br.com.sankhya.BiaLocalServer.Database;
	import br.com.sankhya.BiaLocalServer.RegularExpression;

## Available persistent variables ##
	private boolean $aguardandoConfirmacao = false;
	private boolean $aguardandoParametro = false;
	private boolean $continuar = false;
	private Map<String, String> $memoria = new HashMap<String, String>();
	private Map<String, String> $parametros = new HashMap<String, String>();

## Temporary Variables ##
	BigDecimal $codusu = /* null or defined user code at sudo java -jar */
	String $entrada;
	Database $sql = new Database();
	String $saida = "#verifyOthers#";